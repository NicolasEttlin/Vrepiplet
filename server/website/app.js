// Servir l'exécutable client
var express = require('express'),
    app = express(),
    path = require('path');

app.use(express.static(path.join(__dirname + '/public')));

app.set('view engine', 'pug')

app.get('/', (req, res) => {
  res.render(path.join(__dirname + '/views/index'));
});

app.listen(80, function(){
  console.log("Serveur web lancé");
});
