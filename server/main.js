
var io        = require('socket.io')(21),
    address   = require('address'),
    commandes = require('./commandes');

var users = {};

console.log(`
***************************
*     Vrepiplet server    *
*           v1.0          *
***************************`);

io.on('connection', (socket) => {

  socket.pseudo = "Inconnu";

  // Si l'IP du client est la même que celle du serveur, mettre en admin
  var serverIP = address.ip();
  var clientIP = socket.request.connection.remoteAddress;

  // Mettre admin si l'IP = l'IP du serveur
  socket.isAdmin = ('::ffff:' + serverIP) == clientIP;

  socket.on('version', (v) => {
    if(v !== '0.6.0') {
      socket.emit('message', 'Votre client Vrepiplet est obsolète, veuillez le mettre à jour.');
      socket.disconnect();
    }
  });

  // Quand l'utilisateur change son pseudo
  socket.on('pseudo', (pseudo) => {

    if(pseudo in users) {
      return socket.emit('erreur', 'Le pseudo que vous avez choisi est déjà utilisé.');
    }

    if(socket.pseudo == "Inconnu") {
      io.emit('info', pseudo + ' a rejoint le chat.');
    } else {

      delete users[socket.pseudo];

      if(socket.pseudo !== pseudo)
        io.emit('info', socket.pseudo + " s'appelle désormais " + pseudo);
    }

    socket.pseudo = pseudo;
    users[socket.pseudo] = socket;

    updateusersList();
  });

  // Quand l'utilisateur envoie un message
  socket.on('talk', (message) => {

    if(message[0] == '/') // commande
      return commandes(message, socket, io, users);

    io.emit('user message', {
      text: message,
      pseudo: socket.pseudo
    });
  });

  socket.on('disconnect', function() {
    if(socket.pseudo !== "Inconnu")
      io.emit('info', socket.pseudo + ' a quitté le chat.')

    delete users[socket.pseudo];

    updateusersList();
  });

});

updateusersList = () => {
  var connectes = [];
  for(p in users) {
    connectes.push(p);
  }

  io.emit('people', connectes.sort().join('¢'));
};

// Lancer le serveur qui permet de télécharger l'exécutable
const website = require('./website/app');
