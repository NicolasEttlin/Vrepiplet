
module.exports = (message, socket, io, users) => {

  var commande = message.split(' ')[0];
  var target = message.split(' ')[1];

  switch(commande)
  {
    case '/w': // Message privé
      if(target in users) {
        users[target].emit('user message', {
          pseudo: `[MP] ${socket.pseudo}`,
          text: message.substr(3 + target.length),
          private: true
        });

        socket.emit('user message', {
          pseudo: `[MP] -> ${target}`,
          text: message.substr(3 + target.length),
          private: true
        });
      } else {
        if(typeof target === 'undefined') {
          socket.emit('erreur', 'Syntaxe : /w <pseudo> <message>');
          break;
        }
        socket.emit('erreur', `Le joueur ${target} est introuvable.`);
      }
      break;
    case '/kick':
      if(socket.isAdmin) {
          if(target in users) {
          io.emit('info', `${socket.pseudo} a éjecté ${target} du serveur.`);
          users[target].disconnect();
        } else {
          socket.emit('erreur', `Le joueur ${target} est introuvable.`);
        }
      } else {
        socket.emit('erreur', `Vous devez être administrateur pour éjecter une personne du serveur.`);
      }
      break;
    case '/op': // Rendre un autre utilisateur admin
      if(socket.isAdmin) {
          if(target in users) {
          io.emit('info', `${socket.pseudo} a éjecté ${target} du serveur.`);
          users[target].disconnect();
        } else {
          socket.emit('erreur', `Le joueur ${target} est introuvable.`);
        }
      } else {
        socket.emit('erreur', `Vous devez être administrateur pour utiliser cette commande`);
      }
      break;
    default:
      socket.emit('message', `Votre commande n'a pas été reconnue.`);
  }
};
