﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;

namespace Vrepiplet
{
    public partial class frmChat : Form
    {
        public frmChat()
        {
            InitializeComponent();
        }

        bool OnForeground = true;

        /// <summary>
        /// Ouvrir le chat lorsque l'on clique sur l'icône de notification
        /// </summary>
        private void Show(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
            }

            Activate();
        }

        Socket socket;

        /// <summary>
        /// Connexion
        /// </summary>
        private void btnChoisirPseudo_Click(object sender, EventArgs e)
        {
            if (socket != null)
            {
                // Déjà connecté ? On envoie juste le nouveau pseudo
                socket.Emit("pseudo", tbxPseudo.Text.Trim());

                return;
            }

            socket = IO.Socket("http://10.134.97.129:21");

            // Lors de la connexion
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                // Envoyer notre version du client
                socket.Emit("version", "0.6.0");

                // Envoyer notre pseudo
                socket.Emit("pseudo", tbxPseudo.Text.Trim());

                // Activer les composants du chat
                rtbChat.Invoke((MethodInvoker)delegate
                {
                    rtbChat.Enabled = true;
                    tbxMessage.Enabled = true;
                    btnEnvoyerMessage.Enabled = true;
                    AcceptButton = btnEnvoyerMessage;

                    tbxMessage.Focus();
                });
            });

            socket.On(Socket.EVENT_CONNECT_ERROR, () =>
            {
                addMessageToChat("Impossible de se connecter au serveur.", Color.Red);
            });

            // Lorsqu'un message est reçu
            socket.On("user message", (message) =>
            {
                JObject data = (JObject)message;

                bool isPrivate = data["private"] != null;

                addMessageToChat("<" + data.GetValue("pseudo").ToString() + "> ", Color.DarkCyan, true, isPrivate);
                addMessageToChat(data.GetValue("text").ToString(), Color.Black, false, isPrivate);

                // Notification
                if (!OnForeground)
                {
                    notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                    notifyIcon.BalloonTipTitle = "Message de " + data.GetValue("pseudo");
                    notifyIcon.BalloonTipText = data.GetValue("text").ToString();
                    notifyIcon.ShowBalloonTip(1000);
                }
            });

            socket.On("erreur", (message) =>
            {
                addMessageToChat(message.ToString(), Color.Crimson);
            });

            socket.On("info", (message) =>
            {
                addMessageToChat(message.ToString(), Color.ForestGreen);
            });

            socket.On("message", (message) =>
            {
                addMessageToChat(message.ToString(), Color.Black);
            });

            socket.On("people", (people) =>
            {
                lbxPeople.Invoke((MethodInvoker)delegate
                {
                    lbxPeople.Items.Clear();

                    foreach (string pseudo in people.ToString().Split('¢'))
                        lbxPeople.Items.Add(pseudo);
                });
            });

            socket.On("disconnect", () =>
            {
                addMessageToChat("Vous avez été déconnecté.", Color.Red);

                btnEnvoyerMessage.Invoke((MethodInvoker)delegate
                {
                    tbxMessage.Enabled = false;
                    tbxMessage.Clear();
                    btnEnvoyerMessage.Enabled = false;
                });
            });

        }

        private void btnEnvoyerMessage_Click(object sender, EventArgs e)
        {
            if (tbxMessage.Text.Trim() == "")
                return;

            socket.Emit("talk", tbxMessage.Text.Trim());

            tbxMessage.Clear();
            tbxMessage.Focus();
        }

        private void addMessageToChat(string message,
            Color color,
            bool line = true,
            bool italic = false
        )
        {
            rtbChat.Invoke((MethodInvoker)delegate
            {
                // Retour à la ligne
                if (rtbChat.Text != "" && line)
                {
                    message = Environment.NewLine + message;
                }

                // Ajouter le texte
                rtbChat.SelectionStart = rtbChat.TextLength;
                rtbChat.SelectionLength = 0;

                rtbChat.SelectionColor = color;
                rtbChat.SelectionFont = !italic ? new Font("Segoe UI", 12) : new Font("Segoe UI", 12, FontStyle.Italic);
                rtbChat.AppendText(message);

                // Scroller en bas du chat
                rtbChat.SelectionStart = rtbChat.TextLength;
                rtbChat.SelectionLength = 0;
                rtbChat.ScrollToCaret();
            });
        }

        /// <summary>
        /// Mettre le focus sur le choix de pseudo au démarrage
        /// </summary>
        private void frmChat_Load(object sender, EventArgs e)
        {
            ActiveControl = tbxPseudo;
        }

        /// <summary>
        /// Empêcher la saisie d'espaces dans le pseudo
        /// </summary>
        private void tbxPseudo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Space)
                e.Handled = true;
        }

        /// <summary>
        /// Lorsque le programme est remis au premier plan
        /// </summary>
        private void frmChat_Activated(object sender, EventArgs e)
        {
            OnForeground = true;
        }

        /// <summary>
        /// Lorsque le programme est envoyé à l'arrière-plan
        /// </summary>
        private void frmChat_Deactivate(object sender, EventArgs e)
        {
            OnForeground = false;
        }

        private void lbxPeople_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxMessage.Focus();
            tbxMessage.Text = String.Format("/w {0} {1}", lbxPeople.SelectedItem, tbxMessage.Text);

            tbxMessage.SelectionStart = tbxMessage.TextLength;
            tbxMessage.SelectionLength = 0;
        }
    }
}
