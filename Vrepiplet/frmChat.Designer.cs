﻿namespace Vrepiplet
{
    partial class frmChat
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChat));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.btnEnvoyerMessage = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbxPeople = new System.Windows.Forms.ListBox();
            this.gbxPseudo = new System.Windows.Forms.GroupBox();
            this.tbxPseudo = new System.Windows.Forms.TextBox();
            this.btnChoisirPseudo = new System.Windows.Forms.Button();
            this.rtbChat = new System.Windows.Forms.RichTextBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gbxPseudo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(513, 355);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tbxMessage, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnEnvoyerMessage, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 318);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(507, 34);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tbxMessage
            // 
            this.tbxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxMessage.Enabled = false;
            this.tbxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMessage.Location = new System.Drawing.Point(3, 1);
            this.tbxMessage.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.Size = new System.Drawing.Size(401, 30);
            this.tbxMessage.TabIndex = 4;
            // 
            // btnEnvoyerMessage
            // 
            this.btnEnvoyerMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEnvoyerMessage.Enabled = false;
            this.btnEnvoyerMessage.Location = new System.Drawing.Point(410, 0);
            this.btnEnvoyerMessage.Margin = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.btnEnvoyerMessage.Name = "btnEnvoyerMessage";
            this.btnEnvoyerMessage.Size = new System.Drawing.Size(94, 32);
            this.btnEnvoyerMessage.TabIndex = 5;
            this.btnEnvoyerMessage.Text = "Envoyer";
            this.btnEnvoyerMessage.UseVisualStyleBackColor = true;
            this.btnEnvoyerMessage.Click += new System.EventHandler(this.btnEnvoyerMessage_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.rtbChat, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(507, 309);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.lbxPeople, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.gbxPseudo, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(169, 303);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // lbxPeople
            // 
            this.lbxPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxPeople.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxPeople.FormattingEnabled = true;
            this.lbxPeople.IntegralHeight = false;
            this.lbxPeople.ItemHeight = 21;
            this.lbxPeople.Location = new System.Drawing.Point(3, 85);
            this.lbxPeople.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lbxPeople.Name = "lbxPeople";
            this.lbxPeople.Size = new System.Drawing.Size(163, 218);
            this.lbxPeople.TabIndex = 8;
            this.lbxPeople.SelectedIndexChanged += new System.EventHandler(this.lbxPeople_SelectedIndexChanged);
            // 
            // gbxPseudo
            // 
            this.gbxPseudo.Controls.Add(this.tbxPseudo);
            this.gbxPseudo.Controls.Add(this.btnChoisirPseudo);
            this.gbxPseudo.Location = new System.Drawing.Point(3, 3);
            this.gbxPseudo.Name = "gbxPseudo";
            this.gbxPseudo.Size = new System.Drawing.Size(163, 76);
            this.gbxPseudo.TabIndex = 4;
            this.gbxPseudo.TabStop = false;
            this.gbxPseudo.Text = "Choisissez votre pseudo";
            // 
            // tbxPseudo
            // 
            this.tbxPseudo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPseudo.Location = new System.Drawing.Point(7, 20);
            this.tbxPseudo.MaxLength = 20;
            this.tbxPseudo.Name = "tbxPseudo";
            this.tbxPseudo.Size = new System.Drawing.Size(151, 24);
            this.tbxPseudo.TabIndex = 0;
            this.tbxPseudo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxPseudo_KeyPress);
            // 
            // btnChoisirPseudo
            // 
            this.btnChoisirPseudo.Location = new System.Drawing.Point(7, 50);
            this.btnChoisirPseudo.Name = "btnChoisirPseudo";
            this.btnChoisirPseudo.Size = new System.Drawing.Size(151, 23);
            this.btnChoisirPseudo.TabIndex = 1;
            this.btnChoisirPseudo.Text = "OK";
            this.btnChoisirPseudo.UseVisualStyleBackColor = true;
            this.btnChoisirPseudo.Click += new System.EventHandler(this.btnChoisirPseudo_Click);
            // 
            // rtbChat
            // 
            this.rtbChat.BackColor = System.Drawing.Color.White;
            this.rtbChat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbChat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbChat.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbChat.Location = new System.Drawing.Point(178, 12);
            this.rtbChat.Margin = new System.Windows.Forms.Padding(3, 12, 3, 3);
            this.rtbChat.Name = "rtbChat";
            this.rtbChat.ReadOnly = true;
            this.rtbChat.Size = new System.Drawing.Size(326, 294);
            this.rtbChat.TabIndex = 3;
            this.rtbChat.Text = "";
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Vrepiplet";
            this.notifyIcon.Visible = true;
            this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.Show);
            this.notifyIcon.Click += new System.EventHandler(this.Show);
            // 
            // frmChat
            // 
            this.AcceptButton = this.btnChoisirPseudo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 355);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(465, 287);
            this.Name = "frmChat";
            this.Text = "Vrepiplet";
            this.Activated += new System.EventHandler(this.frmChat_Activated);
            this.Deactivate += new System.EventHandler(this.frmChat_Deactivate);
            this.Load += new System.EventHandler(this.frmChat_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.gbxPseudo.ResumeLayout(false);
            this.gbxPseudo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnEnvoyerMessage;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox gbxPseudo;
        private System.Windows.Forms.TextBox tbxPseudo;
        private System.Windows.Forms.Button btnChoisirPseudo;
        private System.Windows.Forms.ListBox lbxPeople;
        private System.Windows.Forms.RichTextBox rtbChat;
        private System.Windows.Forms.NotifyIcon notifyIcon;

    }
}

