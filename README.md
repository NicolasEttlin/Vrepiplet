![Icône de Vrepiplet](img/logo.png)

# Vrepiplet
> *par Nicolas Ettlin*

💬 Messagerie instantanée avec client pour Windows écrit en C# et serveur écrit en JavaScript.

## Technologies utilisées

### Client
* Visual Studio 2013
* Windows Forms
* Librairie [SocketIoClientDotNet](https://github.com/Quobject/SocketIoClientDotNet/) (pour communiquer avec le serveur socket.io)
* Librairie [Costura.Fody](https://www.nuget.org/packages/Costura.Fody/) (pour intégrer les DLL de SocketIoClientDotNet dans le fichier `.exe`)

### Serveur
* Node.js v7.2.1
* [Socket.io](http://socket.io/)
* [Express.js](http://expressjs.com/) 4.14.0 et [Pug 2.0](https://pugjs.org/) (pour créer un serveur web permettant télécharger l'exécutable de Vrepiplet)

## Fonctionnalités

* Messagerie en temps réel
* Choix de pseudo
* Historique de chat avec coloration syntaxique (`RichTextBox`)
* Commande `/w` pour envoyer un message privé à une personne connectée
* Liste (`ListBox`) des utilisateurs connectés (un clic sur une personne permet de lui envoyer un message privé avec la commande /w)
* Commande `/kick` permettant d'éjecter du serveur un utilisateur. Cette commande est réservée aux administrateurs.
* Système de permissions : une personne reçoit les droits d'administrateur uniquement si son adresse IP est la même que celle du serveur. La commande `/op` (accessible uniquement aux administrateurs) permet de donner les droits d'administrateur à un autre utilisateur.
* Serveur web permettant de télécharger le fichier .exe de Vrepiplet

![Capture d'écran de Vrepiplet](img/screenshot.png)

## Communication client ↔ serveur

Les messages transitent par un serveur Node.js en utilisant la librairie [socket.io](http://socket.io/), qui permet d'échanger des messages entre clients et serveur via *WebSocket*. Le serveur reçoit tous les messages et les envoie en retour aux utilisateurs concernés.

Socket.io est normalement prévu pour communiquer entre un serveur et une page web (ou tout autre client JavaScript). C'est pourquoi j'ai utilisé la librairie [SocketIoClientDotNet](https://github.com/Quobject/SocketIoClientDotNet/), qui est un client socket.io écrit en C#.

![Communication entre le serveur et les clients](img/communication.png)
